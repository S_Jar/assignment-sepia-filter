NAME := image-transformer

##### Compiler / analyzer common configuration.

ASM = nasm
ASM_FLAGS = -felf64
CC = clang
LINKER = $(CC)

RM = rm -rf
MKDIR = mkdir -p

# Clang-tidy
CLANG_TIDY = clang-tidy

_noop =
_space = $(noop) $(noop)
_comma = ,

# Using `+=` to let user define their own checks in command line
CLANG_TIDY_CHECKS += $(strip $(file < clang-tidy-checks.txt))
CLANG_TIDY_ARGS = \
	-warnings-as-errors=* -header-filter="$(abspath $(INCDIR.main))/.*" \
	-checks="$(subst $(_space),$(_comma),$(CLANG_TIDY_CHECKS))" \

# Sanitizers
CFLAGS.none :=
CFLAGS.asan := -fsanitize=address
CFLAGS.lsan := -fsanitize=leak
CFLAGS.msan := -fsanitize=memory -fsanitize-memory-track-origins=2 -fno-omit-frame-pointer -fno-optimize-sibling-calls
CFLAGS.usan := -fsanitize=undefined

SANITIZER ?= none
ifeq ($(SANITIZER),)
override SANITIZER := none
endif

ifeq ($(words $(SANITIZER)),1)
ifeq ($(filter $(SANITIZER),all asan lsan msan usan none),)
$(error Please provide correct argument value for SANITIZER: all, asan, lsan, msan, usan or none)
endif
endif

# Using `+=` to let user define their own flags in command line
CFLAGS += $(CFLAGS.$(SANITIZER))
LDFLAGS += $(CFLAGS.$(SANITIZER))

ifeq ($(SANITIZER),none)
OBJDIR = obj
BUILDDIR = build
else
OBJDIR = obj/$(SANITIZER)
BUILDDIR = build/$(SANITIZER)
endif

##### Configuration for `main` target.

SOLUTION_DIR = solution

SRCDIR.main = $(SOLUTION_DIR)/src
INCDIR.main = $(SOLUTION_DIR)/include
OBJDIR.main = $(OBJDIR)/$(SOLUTION_DIR)

SOURCES.main += $(wildcard $(SRCDIR.main)/*.c) $(wildcard $(SRCDIR.main)/*/*.c) 
SOURCES.main += $(wildcard $(SRCDIR.main)/*.asm) $(wildcard $(SRCDIR.main)/*/*.asm)
TARGET.main  := $(BUILDDIR)/$(NAME)

CFLAGS.main += $(strip $(file < $(SOLUTION_DIR)/compile_flags.txt)) $(CFLAGS) -I$(INCDIR.main)

##### Configuration for `naive` target.

SOLUTION_DIR = solution

SRCDIR.naive = $(SOLUTION_DIR)/src_naive
INCDIR.naive = $(SOLUTION_DIR)/include
OBJDIR.naive = $(OBJDIR)/$(SOLUTION_DIR)_naive

SOURCES.naive += $(wildcard $(SRCDIR.naive)/*.c) $(wildcard $(SRCDIR.naive)/*/*.c) 
TARGET.naive  := $(BUILDDIR)/$(NAME)_naive

CFLAGS.naive += $(strip $(file < $(SOLUTION_DIR)/compile_flags.txt)) $(CFLAGS) -I$(INCDIR.main)

##### Rule templates. Should be instantiated with $(eval $(call template, ...))

# I use $$(var) in some variables to avoid variable expanding too early.
# I do not remember when $(var) is expanded in `define` rules, but $$(var)
# is expanded exactly at $(eval ...) call.

# Template for running submake on each SANITIZER value when SANITIZER=all is used. 
# $(1) - SANITIZER value (none/asan/lsan/msan/usan)
define make-sanitizer-rule

GOALS.$(1) := $$(patsubst %,%.$(1),$$(MAKECMDGOALS))

$$(MAKECMDGOALS): $$(GOALS.$(1))
.PHONY: $$(GOALS.$(1))

$$(GOALS.$(1)):
	@echo Running 'make $$(patsubst %.$(1),%,$$@)' with SANITIZER=$(1)
	@$(MAKE) $$(patsubst %.$(1),%,$$@) SANITIZER=$(1)

endef

# Template for compilation and linking rules + depfile generation and inclusion
# $(1) - target name (main/naive)
define make-compilation-rule

OBJECTS.$(1) := $$(SOURCES.$(1):$$(SRCDIR.$(1))/%.c=$$(OBJDIR.$(1))/%.o)
OBJECTS.$(1) := $$(OBJECTS.$(1):$$(SRCDIR.$(1))/%.asm=$$(OBJDIR.$(1))/%.o)
SRCDEPS.$(1) := $$(OBJECTS.$(1):%.o=%.o.d)

DIRS.$(1) := $$(sort $$(dir $$(OBJECTS.$(1)) $$(TARGET.$(1))))
DIRS += $$(DIRS.$(1))

.PHONY: build-$(1)

build-$(1): $$(TARGET.$(1))

$$(TARGET.$(1)): $$(OBJECTS.$(1)) | $$(DIRS.$(1))
	$(LINKER) $(LDFLAGS) $$(OBJECTS.$(1)) -o $$@

$$(OBJDIR.$(1))/%.o: $$(SRCDIR.$(1))/%.c | $$(DIRS.$(1))
	$(CC) $(CFLAGS.$(1)) -M -MP $$< >$$@.d
	$(CC) $(CFLAGS.$(1)) -c $$< -o $$@

$$(OBJDIR.$(1))/%.o: $$(SRCDIR.$(1))/%.asm | $$(DIRS.$(1))
	$(ASM) $(ASM_FLAGS) $$< -o $$@

-include $$(SRCDEPS.$(1))

endef

##### Rules and targets.

.PHONY: all naive clean check

ifeq ($(MAKECMDGOALS),)
MAKECMDGOALS := all
endif


ifeq ($(SANITIZER),all)
# Do all the work in sub-makes
$(foreach sanitizer,none asan lsan msan usan,$(eval $(call make-sanitizer-rule,$(sanitizer))))
else

all: build-main

naive: build-naive

check:
	$(CLANG_TIDY) $(CLANG_TIDY_ARGS) $(SOURCES.main)

$(foreach target,main naive,$(eval $(call make-compilation-rule,$(target))))

clean:
	$(RM) $(OBJDIR) $(BUILDDIR)

$(sort $(DIRS)):
	$(MKDIR) $@

endif
