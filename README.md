# assignment-8-sepia

## Сборка
* make (all) - собрать версию с SSE
* make naive - собрать версию без SSE


## Результаты
Программа тестировалась на картинке  
https://drive.google.com/file/d/1b_584vPIn5_ZmV9Jc_aawwskFTtmPaR-/view?usp=sharing

### Среднее время выполнения наивного варианта
549998.5 мкс

### Среднее время выполнения варианта с SSE
959483.1 мкс
