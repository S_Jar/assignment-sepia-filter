section .text
global sse

sse:
	movdqa xmm0, [rdi]
	movdqa xmm1, [rsi]
	movdqa xmm2, [rdx]
	movdqa xmm3, [rcx]
	movdqa xmm4, [r8]
	movdqa xmm5, [r9]
	mulps xmm0, xmm3
	mulps xmm1, xmm4
	mulps xmm2, xmm5
	addps xmm0, xmm1
	addps xmm0, xmm2
	cvtps2dq xmm0, xmm0
	packssdw xmm0, xmm0
	packuswb xmm0, xmm0
	pop rsi
	pop rdi
	push rdi
	push rsi
	movdqa [rdi], xmm0
	ret
