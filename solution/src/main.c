#include "BMP.h"
#include "errhandling.h"
#include "fileoc.h"
#include "image.h"
#include "sepia.h"

#include <sys/time.h>
#include <sys/resource.h>

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>

void print_error(const char* err){
	fprintf(stderr, "%s", err);
}


int main( int argc, char** argv ) {
    if(argc != 3 || !argv || !argv[1] || !argv[2]){
	    print_error("usage ./image-transformer <source-image> <transformed-image>");
	    return 1;
    }
    enum open_status o_st;
    enum close_status c_st;
    enum read_status r_st;
    enum write_status w_st;
    struct rusage resources;
    struct timeval start_time;
    struct timeval end_time;
    FILE* old_image = {0};
    o_st = open_for_read(argv[1], &old_image); 
    if(o_st != OPEN_OK){
	    print_error(get_open_err(o_st));
	    return 2;
    }
    struct image image = {0};
    r_st = from_bmp(old_image, &image);
    if(r_st != READ_OK){
	    print_error(get_read_err(r_st));
	    image_destroy(image);
	    return 3;
    }
    c_st = close(old_image);
    if(c_st != CLOSE_OK){
	    print_error(get_close_err(c_st));
	    image_destroy(image);
	    return 4;
    }
    FILE* new_image = {0};
    o_st = open_for_write(argv[2], &new_image);
    if(o_st != OPEN_OK){
	    print_error(get_open_err(o_st));
	    image_destroy(image);
	    return 5;
    }
    struct image n_image = {0};
     getrusage(RUSAGE_SELF, &resources);
    start_time = resources.ru_utime;
    n_image = apply_sepia_filter(image);
    getrusage(RUSAGE_SELF, &resources);
    end_time = resources.ru_utime;
    image_destroy(image);
    w_st =to_bmp(new_image, &n_image);
    if(w_st != WRITE_OK){
	    print_error(get_write_err(w_st));
	    image_destroy(n_image);
	    return 6;
    }
    c_st = close(new_image);
    if(c_st != CLOSE_OK){
	    print_error(get_close_err(c_st));
	    image_destroy(n_image);
	    return 7;
    }
    image_destroy(n_image);
    long work_time = ((end_time.tv_sec - start_time.tv_sec)*1000000L) + end_time.tv_usec - start_time.tv_usec;
    printf("Program worked %ld microseconds", work_time);
    return 0;
}


