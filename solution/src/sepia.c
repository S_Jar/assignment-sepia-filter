#include "sepia.h"
#include "image.h"
#include "util.h"
#include <inttypes.h>
#include <stdint.h>
#include <string.h>

static const float c[3][3] = {
	{ .393f, .769f, .189f },
	{ .349f, .686f, .168f },
	{ .272f, .543f, .131f } 
};

static unsigned char sat(uint64_t x){
	if(x < 256) return x; return 255;
}

static void sepia_one( struct pixel* const pixel){	
	struct pixel const old = *pixel;

	pixel->r = sat(byte_to_float[old.r] * c[0][0] + byte_to_float[old.g] * c[0][1] + byte_to_float[old.b] * c[0][2]);
	pixel->g = sat(byte_to_float[old.r] * c[1][0] + byte_to_float[old.g] * c[1][1] + byte_to_float[old.b] * c[1][2]);
	pixel->b = sat(byte_to_float[old.r] * c[2][0] + byte_to_float[old.g] * c[2][1] + byte_to_float[old.b] * c[2][2]);
}

void  sse( float[static 4], float[static 4], float[static 4], float[static 4], 
		float[static 4], float[static 4], uint8_t[static 16] );

static void sepia_sse(struct pixel* chunk[4]){
	float xmm0[4] = {0}, xmm1[4] = {0}, xmm2[4] = {0},
	      xmm3[4] = {0}, xmm4[4] = {0}, xmm5[4] = {0};
	uint8_t cnt = 0;
	uint8_t chunk_pos = 0;
	uint8_t c_pos = 0;
	uint8_t results[3][16] = {0};
	
	for(uint8_t i = 0; i < 3; i++){
		for(uint8_t j = 0; j < 4; j++){
			if(cnt > 2){
				cnt = 0;
				chunk_pos++;
			}
			xmm0[j] = byte_to_float[chunk[chunk_pos]->b];
			xmm1[j] = byte_to_float[chunk[chunk_pos]->g];
			xmm2[j] = byte_to_float[chunk[chunk_pos]->r];
			cnt++;
			xmm3[j] = c[c_pos%3][0];
			xmm4[j] = c[c_pos%3][1];
			xmm5[j] = c[c_pos%3][2];
			c_pos++;
		}
		sse(xmm0, xmm1, xmm2, xmm3, xmm4, xmm5, results[i]);
	}
	chunk[0]->r = results[0][0]; chunk[0]->g = results[0][1]; chunk[0]->b = results[0][2];
	chunk[1]->r = results[0][3]; chunk[1]->g = results[1][0]; chunk[1]->b = results[1][1];
	chunk[2]->r = results[1][2]; chunk[2]->g = results[1][3]; chunk[2]->b = results[2][0];
	chunk[3]->r = results[2][1]; chunk[3]->g = results[2][2]; chunk[3]->b = results[2][3];
}

static void sepia_c_inplace(struct image* img){
	struct pixel* chunk[4] = {0};
	uint64_t x = 0;
	for(uint64_t i = 3; i < (img->height)*(img->width); i+=4){
		chunk[0] = &img->data[i-3];
		chunk[1] = &img->data[i-2];
		chunk[2] = &img->data[i-1];
		chunk[3] = &img->data[i];
		sepia_sse(chunk);
		x = i;
	}
	for(; x < (img->width)*(img->height); x++)
		sepia_one(&img->data[x]);
}
struct image apply_sepia_filter(struct image source){
	struct image new_image = image_init(source.width, source.height);
	memcpy(new_image.data, source.data, source.width * source.height * 
			sizeof(struct pixel));
	sepia_c_inplace(&new_image);
	return new_image;
}
