#ifndef _SEPIA_H_
#define _SEPIA_H_
#include "image.h"
struct image apply_sepia_filter(struct image source);
#endif
