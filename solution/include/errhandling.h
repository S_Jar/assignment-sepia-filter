#ifndef _ERRHANDLING_H_
#define _ERRHANDLING_H_
#include "BMP.h"
#include "fileoc.h"

const char* get_open_err(enum open_status);

const char* get_close_err(enum close_status);

const char* get_read_err(enum read_status);

const char* get_write_err(enum write_status);

#endif
