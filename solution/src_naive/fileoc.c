#include <fileoc.h>

enum open_status open_for_read(const char *pathname, FILE **file){
	if(!pathname)
		return OPEN_ERROR;
	FILE *f = fopen(pathname, "r");
	if(f){
		*file = f;
		return OPEN_OK;
	}
	return OPEN_ERROR;
}

enum open_status open_for_write(const char* pathname, FILE **file){
	if(!pathname)
		return OPEN_ERROR;
	FILE *f = fopen(pathname, "w");
	if(f){
		*file = f;
		return OPEN_OK;
	}
	return OPEN_ERROR;
}

enum close_status close(FILE *file){
	if(!file)
		return CLOSE_ERROR;
	int status = fclose(file);
	if(status == 0)
		return CLOSE_OK;
	return CLOSE_ERROR;
}
