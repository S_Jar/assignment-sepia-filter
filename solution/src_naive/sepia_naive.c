#include "sepia.h"
#include "image.h"
#include "util.h"
#include <inttypes.h>
#include <stdint.h>
#include <string.h>
static unsigned char sat(uint64_t x){
	if(x < 256) return x; return 255;
}

static void sepia_one( struct pixel* const pixel){
	static const float c[3][3] = {
		{ .393f, .769f, .189f },
		{ .349f, .686f, .168f },
		{ .272f, .543f, .131f } 
	};
	struct pixel const old = *pixel;

	pixel->r = sat(byte_to_float[old.r] * c[0][0] + byte_to_float[old.g] * c[0][1] + byte_to_float[old.b] * c[0][2]);
	pixel->g = sat(byte_to_float[old.r] * c[1][0] + byte_to_float[old.g] * c[1][1] + byte_to_float[old.b] * c[1][2]);
	pixel->b = sat(byte_to_float[old.r] * c[2][0] + byte_to_float[old.g] * c[2][1] + byte_to_float[old.b] * c[2][2]);
}

static void sepia_c_inplace(struct image* img){
	uint32_t x, y;
	for(y = 0; y < img->height; y++)
		for(x = 0; x < img->width; x++)
			sepia_one( pixel_at( *img, x, y ) );
}
struct image apply_sepia_filter(struct image source){
	struct image new_image = image_init(source.width, source.height);
	memcpy(new_image.data, source.data, source.width * source.height * 
			sizeof(struct pixel));
	sepia_c_inplace(&new_image);
	return new_image;
}
